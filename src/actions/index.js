import axios from 'axios';
import { toastr } from 'react-redux-toastr';
import * as types from './actionTypes';

const ROOT_URL = 'http://test-api.kuria.tshdev.io/';

function fetchSuppliersSuccess(response) {
  return {
    type: types.FETCH_SUPPLIERS_SUCCESS,
    payload: response.data,
  };
}

// Application won't execute this function when no payments
// because backend response 'An error occured!' but with 200 HTTP Status
function fetchSuppliersFailure() {
  toastr.error('Something went wrong', 'Contact with administrator');
  return {
    type: types.FETCH_SUPPLIERS_FAILURE,
  };
}

export function fetchSuppliers(query, rating) {
  return dispatch =>
    axios
      .get(`${ROOT_URL}?query=${query}&rating=${rating}`)
      .then(res => dispatch(fetchSuppliersSuccess(res)))
      .catch(err => dispatch(fetchSuppliersFailure(err)));
}

function fetchPageSuccess(response) {
  return {
    type: types.FETCH_SUPPLIERS_SUCCESS,
    payload: response.data,
  };
}

function fetchPageFailure(error) {
  toastr.error('Something went wrong', 'Contact with administrator');
  return {
    type: types.FETCH_SUPPLIERS_FAILURE,
    error,
  };
}

export function fetchPage(params) {
  return dispatch =>
    axios
      .get(`${ROOT_URL}?${params}`)
      .then(res => dispatch(fetchPageSuccess(res)))
      .catch(err => dispatch(fetchPageFailure(err)));
}

export function resetSearch() {
  return {
    type: types.RESET_SEARCH,
  };
}

export function selectPayment(paymentDetails) {
  return {
    type: types.SELECT_PAYMENT,
    paymentDetails,
  };
}

export function resetSelectedPayment() {
  return {
    type: types.RESET_SELECTED_PAYMENT,
  };
}
