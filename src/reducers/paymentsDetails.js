import * as types from '../actions/actionTypes';

export const INITIAL_STATE = {
  selectedPayment: null,
  payments: [],
  pagination: {},
};

export default function (state = INITIAL_STATE, action) {
  switch (action.type) {
    case types.FETCH_SUPPLIERS_SUCCESS:
    case types.FETCH_PAGE_SUCCESS:
      const { payments, pagination } = action.payload;
      return {
        ...state,
        payments,
        pagination,
      };
    case types.SELECT_PAYMENT:
      return {
        ...state,
        selectedPayment: action.paymentDetails,
      };
    case types.RESET_SELECTED_PAYMENT:
      return {
        ...state,
        selectedPayment: null,
      };
    case types.FETCH_SUPPLIERS_FAILURE:
    case types.FETCH_PAGE_FAILURE:
    case types.RESET_SEARCH:
      return INITIAL_STATE;
    default:
      return state;
  }
}
