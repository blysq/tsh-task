import { combineReducers } from 'redux';
import { reducer as toastrReducer } from 'react-redux-toastr';
import paymentsDetails from './paymentsDetails';

const rootReducer = combineReducers({
  toastr: toastrReducer,
  paymentsDetails,
});

export default rootReducer;
