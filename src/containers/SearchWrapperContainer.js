import { connect } from 'react-redux';
import SearchWrapper from '../components/SearchWrapper/SearchWrapper';
import {
  fetchSuppliers,
  fetchPage,
  resetSearch,
  selectPayment,
  resetSelectedPayment,
} from '../actions/index';

function mapStateToProps(state) {
  return {
    selectedPayment: state.paymentsDetails.selectedPayment,
    payments: state.paymentsDetails.payments,
    pagination: state.paymentsDetails.pagination,
  };
}

const mapDispatchToProps = dispatch => {
  return {
    fetchSuppliers: (query, rating) => {
      dispatch(fetchSuppliers(query, rating));
    },
    fetchPage: (params) => {
      dispatch(fetchPage(params));
    },
    resetSearch: () => {
      dispatch(resetSearch());
    },
    selectPayment: (payment) => {
      dispatch(selectPayment(payment));
    },
    resetSelectedPayment: () => {
      dispatch(resetSelectedPayment());
    },
  };
};

const SearchWrapperContainer = connect(mapStateToProps, mapDispatchToProps)(SearchWrapper);

export default SearchWrapperContainer;
