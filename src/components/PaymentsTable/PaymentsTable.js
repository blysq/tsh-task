import React from 'react';
import PropTypes from 'prop-types';
import TableElement from '../TableElement/TableElement';
import './PaymentsTable.scss';

const PaymentsTable = ({ payments, selectPayment }) => (
  <table className="payments-table">
    <thead>
      <tr className="payments-table__header">
        <th>Supplier</th>
        <th>Pound Rating</th>
        <th>Reference</th>
        <th>Value</th>
      </tr>
    </thead>
    <tbody>
      {payments &&
        payments.map(payment => (
          <TableElement
            key={payment.payment_ref}
            clickHandler={() => selectPayment(payment)}
            {...payment}
          />
        ))}
    </tbody>
  </table>
);

PaymentsTable.propTypes = {
  payments: PropTypes.array,
  selectPayment: PropTypes.func.isRequired,
};

export default PaymentsTable;
