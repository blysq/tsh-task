import React from 'react';
import PropTypes from 'prop-types';
import RatingLevel from '../RatingLevel/RatingLevel';
import './TableElement.scss';

const TableElement = ({
  payment_amount: amount,
  payment_cost_rating: costRating,
  payment_ref: paymentRef,
  payment_supplier: supplier,
  clickHandler,
}) => (
  <tr onClick={clickHandler} className="table-element text-center">
    <td>{supplier}</td>
    <td>
      <RatingLevel costRating={+costRating} />
    </td>
    <td>{paymentRef}</td>
    <td>&#163;{amount}</td>
  </tr>
);

TableElement.propTypes = {
  payment_amount: PropTypes.string.isRequired,
  payment_cost_rating: PropTypes.string.isRequired,
  payment_ref: PropTypes.string.isRequired,
  payment_supplier: PropTypes.string.isRequired,
  clickHandler: PropTypes.func.isRequired,
};

export default TableElement;
