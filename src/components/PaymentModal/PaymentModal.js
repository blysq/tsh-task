import React from 'react';
import Modal from 'react-modal';
import RatingLevel from '../RatingLevel/RatingLevel';
import './PaymentModal.scss';

const PaymentModal = ({ payment, closeModal }) =>
  payment && (
    <Modal
      className="payment-modal"
      overlayClassName="payment-modal__overlay"
      isOpen={!!payment}
      onRequestClose={closeModal}
      ariaHideApp={false}
    >
      <h3 className="payment-modal__header">
        {payment.payment_supplier} (ref: {payment.payment_ref})
      </h3>
      <RatingLevel costRating={+payment.payment_cost_rating} />
      <p>Payment amount: &#163;{payment.payment_amount}</p>
    </Modal>
  );

export default PaymentModal;
