import React from 'react';
import thunk from 'redux-thunk';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import ReduxToastr from 'react-redux-toastr';
import reducers from '../../reducers';
import PaymentsSummary from '../PaymentsSummary/PaymentsSummary';
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css';

const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);

const App = () => (
  <Provider store={createStoreWithMiddleware(reducers)}>
    <div>
      <PaymentsSummary />
      <ReduxToastr />
    </div>
  </Provider>
);

export default App;
