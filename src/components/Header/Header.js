import React from 'react';
import './Header.scss';

const Intro = () => (
  <div className="intro text-center">
    <h1 className="intro__header">Where your money goes</h1>
    <p className="intro__desc">
      Payments made by Chriester District Council to individual suppliers with value over &#163;500
      made within October.
    </p>
  </div>
);

export default Intro;
