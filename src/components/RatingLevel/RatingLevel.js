import React from 'react';
import PropTypes from 'prop-types';
import { times } from 'underscore';
import poundActiveIcon from '../../assets/pound-active.png';
import poundInactiveIcon from '../../assets/pound-inactive.png';
import './RatingLevel.scss';

const RatingLevel = ({ costRating }) => (
  <div className="rating-level">
    {times(5, n => {
      return n >= costRating ? (
        <img key={n} src={poundInactiveIcon} className="rating-level__icon" alt="White pound icon" />
      ) : (
        <img key={n} src={poundActiveIcon} className="rating-level__icon" alt="Blue pound icon" />
      );
    })}
  </div>
);

RatingLevel.propTypes = {
  costRating: PropTypes.number.isRequired,
};

export default RatingLevel;
