import React from 'react';
import PropTypes from 'prop-types';
import SelectWrapper from '../SelectWrapper/SelectWrapper';
import { toastr } from 'react-redux-toastr';
import './SearchBar.scss';

class SearchBar extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      query: '',
      rating: 0,
    };

    this.handleQueryChange = ::this.handleQueryChange;
    this.handleRatingChange = ::this.handleRatingChange;
    this.handleKeyDown = ::this.handleKeyDown;
    this.resetOptions = ::this.resetOptions;
    this.search = ::this.search;
  }

  handleQueryChange(event) {
    this.setState({
      ...this.state,
      query: event.target.value,
    });
  }

  handleRatingChange(event) {
    this.setState({
      ...this.state,
      rating: event.target.value,
    });
  }

  handleKeyDown(event) {
    if (event.key === 'Enter') {
      this.search();
    }
  }

  resetOptions() {
    this.setState({
      query: '',
      rating: 0,
    });
    this.props.resetHandler();
  }

  search() {
    const { searchHandler } = this.props;

    if (this.state.rating === 0) {
      toastr.warning('Please select pound rating first');
    } else {
      searchHandler(this.state.query, this.state.rating);
    }
  }

  render() {
    return (
      <div className="search-bar">
        <input
          className="search-bar__input"
          type="text"
          placeholder="Search suppliers"
          value={this.state.query}
          onChange={this.handleQueryChange}
          onKeyDown={this.handleKeyDown}
        />
        <SelectWrapper value={+this.state.rating} handleChange={this.handleRatingChange} />
        <button className="btn btn--grey" type="button" onClick={this.resetOptions}>
          Reset
        </button>
        <button className="btn btn--blue" type="button" onClick={this.search}>
          Search
        </button>
      </div>
    );
  }
}

SearchBar.propTypes = {
  resetHandler: PropTypes.func.isRequired,
  searchHandler: PropTypes.func.isRequired,
};

export default SearchBar;
