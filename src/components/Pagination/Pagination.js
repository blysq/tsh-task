import React from 'react';
import PropTypes from 'prop-types';
import { times } from 'underscore';
import './Pagination.scss';

const Pagination = ({ left, right, current, links, total, changePageHandler }) => {
  current = Number(current);
  return (
    <div className="pagination">
      {left && (
        <button
          type="button"
          className="pagination__element"
          onClick={() => changePageHandler(links[current - 1])}
        >
          &lt;
        </button>
      )}
      {current > -1 &&
        times(4, n => {
          const pageNumber = current < 4 ? n : current + n;
          if (pageNumber >= total) {
            return null;
          }

          return (
            <button
              type="button"
              key={n}
              className={`pagination__element ${
                pageNumber === current ? 'pagination--active' : ''
              }`}
              onClick={() => changePageHandler(links[pageNumber])}
            >
              {pageNumber + 1}
            </button>
          );
        })}
      {right && (
        <button
          type="button"
          className="pagination__element"
          onClick={() => changePageHandler(links[current + 1])}
        >
          &gt;
        </button>
      )}
    </div>
  );
};

Pagination.propTypes = {
  left: PropTypes.bool,
  right: PropTypes.bool,
  total: PropTypes.number,
  changePageHandler: PropTypes.func,
};

export default Pagination;
