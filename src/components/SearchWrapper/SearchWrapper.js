import React from 'react';
import PropTypes from 'prop-types';
import SearchBar from '../SearchBar/SearchBar';
import PaymentsTable from '../PaymentsTable/PaymentsTable';
import Pagination from '../Pagination/Pagination';
import PaymentModal from '../PaymentModal/PaymentModal';

const SearchWrapper = props => (
  <React.Fragment>
    <SearchBar
      resetHandler={props.resetSearch}
      searchHandler={(query, rating) => props.fetchSuppliers(query, rating)}
    />
    <PaymentsTable
      selectPayment={payment => props.selectPayment(payment)}
      payments={props.payments}
    />
    <Pagination changePageHandler={page => props.fetchPage(page)} {...props.pagination} />
    <PaymentModal payment={props.selectedPayment} closeModal={props.resetSelectedPayment} />
  </React.Fragment>
);

SearchWrapper.propTypes = {
  resetSearch: PropTypes.func.isRequired,
  fetchSuppliers: PropTypes.func.isRequired,
  fetchPage: PropTypes.func.isRequired,
  resetSelectedPayment: PropTypes.func.isRequired,
  payments: PropTypes.array,
  pagination: PropTypes.object,
  selectedPayment: PropTypes.object,
};

export default SearchWrapper;
