import React from 'react';
import PropTypes from 'prop-types';
import './SelectWrapper.scss';

const SelectWrapper = ({ value, handleChange }) => (
  <div className="select-wrapper">
    <select className="select-wrapper__select" type="number" value={value} onChange={handleChange}>
      <option value="0" disabled>
        Select pound rating
      </option>
      <option value="1">1</option>
      <option value="2">2</option>
      <option value="3">3</option>
      <option value="4">4</option>
      <option value="5">5</option>
    </select>
  </div>
);

SelectWrapper.propTypes = {
  value: PropTypes.number.isRequired,
  handleChange: PropTypes.func.isRequired,
};

export default SelectWrapper;
