import React from 'react';
import Header from '../Header/Header';
import SearchWrapperContainer from '../../containers/SearchWrapperContainer';
import './PaymentsSummary.scss';

const PaymentsSummary = () => (
  <section className="payments">
    <Header />
    <SearchWrapperContainer />
  </section>
);

export default PaymentsSummary;
